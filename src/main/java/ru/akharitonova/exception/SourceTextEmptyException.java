package ru.akharitonova.exception;

public class SourceTextEmptyException extends AbstractTranslateException {

    public SourceTextEmptyException() {
        super("Error! Source text is empty");
    }

}
