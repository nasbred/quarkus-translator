package ru.akharitonova.exception;

public class AbstractTranslateException extends Exception {

    public AbstractTranslateException(String message) {
        super(message);
    }

}
