package ru.akharitonova.exception;

public class YandexEmptyResponseException extends AbstractTranslateException {

    public YandexEmptyResponseException() {
        super("Error! Response from Yandex is null!");
    }

}
