package ru.akharitonova.api;

import io.quarkus.rest.client.reactive.ClientExceptionMapper;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import ru.akharitonova.model.yandex.TranslationRequest;
import ru.akharitonova.model.yandex.TranslationResponse;


import javax.ws.rs.POST;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;

@RegisterRestClient(configKey = "translation-api")
@ClientHeaderParam(name = "Authorization", value = "{lookupAuth}")
@ClientHeaderParam(name = "Content-Type", value = "application/json")
public interface IYandexTranslateService {

    String token = ConfigProvider.getConfig().getValue("yandex.iam.token", String.class);
    
    @POST
    TranslationResponse translate(TranslationRequest message);

    default String lookupAuth() {
        byte[] array = ("Bearer " + token).getBytes(StandardCharsets.UTF_8);
        return new String(array, StandardCharsets.UTF_8);
    }

    @ClientExceptionMapper
    static RuntimeException toException(Response response) {
        if (response.getStatus() != 200) {
            return new RuntimeException("The remote service responded with HTTP " + response.getStatus() + " : " + response.getStatusInfo().getReasonPhrase() + ". ");
        }
        return null;
    }

}
