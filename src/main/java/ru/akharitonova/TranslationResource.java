package ru.akharitonova;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.mutiny.pgclient.PgPool;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jetbrains.annotations.NotNull;
import ru.akharitonova.api.IYandexTranslateService;
import ru.akharitonova.exception.SourceTextEmptyException;
import ru.akharitonova.exception.AbstractTranslateException;
import ru.akharitonova.exception.YandexEmptyResponseException;
import ru.akharitonova.model.TranslatedWordEntity;
import ru.akharitonova.model.TranslationEntity;
import ru.akharitonova.model.yandex.TranslationRequest;
import ru.akharitonova.model.yandex.TranslationResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.stream.Collectors;


@Path("/qt")
public class TranslationResource {

    @RestClient
    IYandexTranslateService yandexTranslateService;

    @Inject
    PgPool client;

    @POST
    @Path("/translate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public TranslationResponse translate(
            String src_text,
            @Context HttpServerRequest httpRequest
    ) throws AbstractTranslateException
    {
//      Prepare data
        if (src_text == null || src_text.isEmpty()) throw new SourceTextEmptyException();
        String language = src_text.contains(";") ? src_text.substring(src_text.indexOf(";") + 2) : "en";
        src_text = src_text.contains(";") ? src_text.substring(0, src_text.indexOf(";")): src_text;
        String[] src_array = src_text.split(" ");
        if (src_text.isEmpty() || src_array.length == 0) throw new SourceTextEmptyException();

//      Translate data
        TranslationRequest request;
        TranslationResponse response;
        TranslationResponse finalResponse = new TranslationResponse();
        String ip = httpRequest.remoteAddress().toString();

        for (String str : src_array) {
            request = new TranslationRequest(new String[]{str}, language);
            response = yandexTranslateService.translate(request);
            if (response == null) throw new YandexEmptyResponseException();

            TranslatedWordEntity word = new TranslatedWordEntity(str, response.getTranslations()[0].getDetectedLanguageCode(), response.getTranslations()[0].getText(), language);
            createDBWord(word).subscribe().asCompletionStage();

            finalResponse.addTranslations(response.getTranslations());
        }

        String tr_text = Arrays.stream(finalResponse.getTranslations()).map(s -> s.getText()).collect(Collectors.joining(" "));
        TranslationEntity translationEntity = new TranslationEntity(ip, src_text, finalResponse.getTranslations()[0].getDetectedLanguageCode(), tr_text, language);
        createDBTrEntity(translationEntity).subscribe().asCompletionStage();

        return finalResponse;
    }

    public Uni<Response> createDBWord(@NotNull TranslatedWordEntity word) {
        return word.save(client).onItem().transform(uri -> Response.ok().build());
    }

    public Uni<Response> createDBTrEntity(@NotNull TranslationEntity entity) {
        return entity.save(client).onItem().transform(uri -> Response.ok().build());
    }

    public Uni<Response> getDBWord(String word) {
        return TranslatedWordEntity.findByWord(client, word)
              .onItem().transform(w -> w != null
                      ? Response.ok(w)
                      : Response.status(Response.Status.NOT_FOUND))
               .onItem().transform(Response.ResponseBuilder::build);
    }

    @GET
    @Path("/all/words")
    public Multi<TranslatedWordEntity> getAllWords() {
        return TranslatedWordEntity.findAll(client);
    }

    @GET
    @Path("/all/translations")
    public Multi<TranslationEntity> getAllTranslations() {
        return TranslationEntity.findAll(client);
    }

}