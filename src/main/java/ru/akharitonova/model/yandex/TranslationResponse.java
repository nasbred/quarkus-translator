package ru.akharitonova.model.yandex;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TranslationResponse {

    private Translation[] translations;

    public Translation[] getTranslations() {
        return this.translations;
    }

    public TranslationResponse() {
    }

    public TranslationResponse(Translation[] translations) {
        this.translations = translations;
    }

    public void addTranslations(Translation[] addedTranslations) {
        int len = this.translations == null ? 0 : this.translations.length;
        Translation[] initialArray;

        if (this.translations != null) initialArray = this.translations;
        else initialArray = new Translation[0];
        Translation[] result = new Translation[len + addedTranslations.length];

        System.arraycopy(initialArray, 0, result, 0, len);
        System.arraycopy(addedTranslations, 0, result, len, addedTranslations.length);

        this.translations = result;
    }

}

