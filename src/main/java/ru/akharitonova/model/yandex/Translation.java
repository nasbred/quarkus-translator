package ru.akharitonova.model.yandex;

public class Translation {
    private String text;
    private String detectedLanguageCode;

    public Translation() {
    }

    public Translation(String text, String detectedLanguageCode) {
        this.text = text;
        this.detectedLanguageCode = detectedLanguageCode;
    }

    public String getText() {
        return text;
    }

    public String getDetectedLanguageCode() {
        return detectedLanguageCode;
    }

}
