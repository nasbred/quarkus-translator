package ru.akharitonova.model.yandex;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.config.ConfigProvider;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Getter
@Setter
@JsonPropertyOrder({"folderId", "texts", "targetLanguageCode"})
public class TranslationRequest {

    private String folderId = ConfigProvider.getConfig().getValue("yandex.folder_id", String.class);
    private String[] texts;
    private String targetLanguageCode;

    public TranslationRequest(String[] texts, String targetLanguageCode) {
        this.texts = texts;
        this.targetLanguageCode = Objects.requireNonNullElse(targetLanguageCode, "en");
    }

    public TranslationRequest(String[] texts) {
        this.texts = texts;
        this.targetLanguageCode = "en";
    }

}