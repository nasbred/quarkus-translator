package ru.akharitonova.model;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.SqlClient;
import io.vertx.mutiny.sqlclient.Tuple;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

@Getter
@Setter
public class TranslatedWordEntity {

    private Long id;
    private String src_word;
    private LocalDateTime created;
    private String src_lang;
    private String tr_word;
    private String tr_lang;

    public TranslatedWordEntity(
            String src_word,
            String src_lang,
            String tr_word,
            String tr_lang
    ) {
        this.created = LocalDateTime.now();
        this.src_word = src_word;
        this.src_lang = src_lang;
        this.tr_word = tr_word;
        this.tr_lang = tr_lang;
    }

    public TranslatedWordEntity(
            Long id,
            LocalDateTime created,
            String src_word,
            String src_lang,
            String tr_word,
            String tr_lang
    ) {
        this.id = id;
        this.created = created;
        this.src_word = src_word;
        this.src_lang = src_lang;
        this.tr_word = tr_word;
        this.tr_lang = tr_lang;
    }

    public static TranslatedWordEntity from(@NotNull Row row) {
        return new TranslatedWordEntity(
                row.getLong("id"),
                row.getLocalDateTime("created"),
                row.getString("src_word"),
                row.getString("src_lang"),
                row.getString("tr_word"),
                row.getString("tr_lang")
        );
    }

    public static Uni<TranslatedWordEntity> findByWord(SqlClient client, String src_word) {
        return client.preparedQuery("SELECT id, created, src_word, src_lang, tr_word, tr_lang" +
                        " FROM tr_word WHERE src_word = $1")
                .execute(Tuple.of(src_word))
                .onItem().transform(RowSet::iterator)
                .onItem().transform(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }

    public Uni<Long> save(PgPool client) {
        LocalDateTime created = LocalDateTime.now();
        String sql = "INSERT INTO tr_word (created, src_word, src_lang, tr_word, tr_lang) " +
                "VALUES ($1, $2, $3, $4, $5) RETURNING id";
        return client.preparedQuery(sql)
                .execute(Tuple.of(created, src_word, src_lang, tr_word, tr_lang))
                .onItem()
                .transform(m -> m.iterator().next().getLong("id"));
    }

    public static Multi<TranslatedWordEntity> findAll(PgPool client) {
        return client.query(
                        "SELECT id, created, src_word, src_lang, tr_word, tr_lang " +
                                "FROM tr_word ORDER BY created ASC")
                .execute()
                .onItem()
                .transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem()
                .transform(TranslatedWordEntity::from);
    }

}
