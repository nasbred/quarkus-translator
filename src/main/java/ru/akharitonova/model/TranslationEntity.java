package ru.akharitonova.model;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.SqlClient;
import io.vertx.mutiny.sqlclient.Tuple;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class TranslationEntity {

    private Long id;
    private LocalDateTime created;
    private String ip_address;
    private String src_message;
    private String src_lang;
    private String tr_message;
    private String tr_lang;

    public TranslationEntity() {
    }

    public TranslationEntity(
            String ip_address,
            String src_message,
            String src_lang,
            String tr_message,
            String tr_lang
    ) {
        this.ip_address = ip_address;
        this.created = LocalDateTime.now();
        this.src_message = src_message;
        this.src_lang = src_lang;
        this.tr_message = tr_message;
        this.tr_lang = tr_lang;
    }

    public TranslationEntity(
            Long id,
            String ip_address,
            LocalDateTime created,
            String src_message,
            String src_lang,
            String tr_message,
            String tr_lang
    ) {
        this.id = id;
        this.ip_address = ip_address;
        this.created = created;
        this.src_message = src_message;
        this.src_lang = src_lang;
        this.tr_message = tr_message;
        this.tr_lang = tr_lang;
    }

    public static TranslationEntity from(Row row) {
        return new TranslationEntity(
                row.getLong("id"),
                row.getString("ip_address"),
                row.getLocalDateTime("created"),
                row.getString("src_message"),
                row.getString("src_lang"),
                row.getString("tr_message"),
                row.getString("tr_lang")
        );
    }

    public static Uni<TranslationEntity> findBySrcMessage(SqlClient client, String src_message) {
        return client.preparedQuery("SELECT id, ip_address, created, src_message, src_lang, tr_message " +
                        "FROM tr_entity WHERE src_message = $1")
                .execute(Tuple.of(src_message))
                .onItem().transform(RowSet::iterator)
                .onItem().transform(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }

    public Uni<Long> save(SqlClient client) {
        String sql = "INSERT INTO tr_entity (ip_address, created, src_message, src_lang, tr_message, tr_lang) " +
                "VALUES ($1, $2, $3, $4, $5, $6) RETURNING id";
        return client.preparedQuery(sql).execute(Tuple.of(
                        ip_address, created, src_message, src_lang, tr_message, tr_lang))
                .onItem().transform(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    public static Multi<TranslationEntity> findAll(PgPool client) {
        return client.query(
                        "SELECT id, ip_address, created, src_message, src_lang, tr_message, tr_lang " +
                                "FROM tr_entity ORDER BY created ASC")
                .execute()
                .onItem()
                .transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem()
                .transform(TranslationEntity::from);
    }

}